name := "liza"

version := "1.0-SNAPSHOT"

libraryDependencies ++= Seq(
  javaJdbc,
  javaEbean,
  cache,
  "org.projectlombok" % "lombok" % "1.16.4"
)

play.Project.playJavaSettings
