package skeletons;


import lombok.Getter;
import lombok.Setter;

/**
 * Created by rishabh.garg on 9/10/16.
 */

@Getter
@Setter
public class UserRequest {

  private String userId;

  private QueryType queryType;

  private String message;

}
