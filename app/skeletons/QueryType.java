package skeletons;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Created by rishabh.garg on 9/10/16.
 */

@AllArgsConstructor
@Getter
public enum QueryType {

  TEXT(0),

  LOCATION(1),

  IMAGE(2);

  private final int value;

}
