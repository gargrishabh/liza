package exception;

/**
 * Created by rishabh.garg on 9/10/16.
 */
public class NotFoundException extends BaseException {

  public NotFoundException(String s) {
    super(s);
  }
}
