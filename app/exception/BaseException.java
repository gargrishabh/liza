package exception;

/**
 * Created by rishabh.garg on 9/10/16.
 */
public abstract class BaseException extends Exception {

  public BaseException(String s) {
    super(s);
  }
}

