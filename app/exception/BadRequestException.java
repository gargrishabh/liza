package exception;

/**
 * Created by rishabh.garg on 9/10/16.
 */
public class BadRequestException extends BaseException {

  public BadRequestException(String s) {
    super(s);
  }
}
