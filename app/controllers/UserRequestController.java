package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import exception.BadRequestException;
import play.libs.F;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import skeletons.UserRequest;
import utils.Utils;
import validation.ValidationCode;

/**
 * Created by rishabh.garg on 9/10/16.
 */
public class UserRequestController extends Controller {

  @BodyParser.Of(BodyParser.Json.class)
  public static F.Promise<Result> processRequest() throws BadRequestException {
    final JsonNode body = request().body().asJson();
    if (body == null) {
      throw new BadRequestException(ValidationCode.INVALID_REQUEST);
    }
    UserRequest userRequest = null;
    F.Promise<Result> response = F.Promise.promise(new F.Function0<Result>() {
      @Override
      public Result apply() throws BadRequestException {
        UserRequest userRequest = null;
        try {
          userRequest = Utils.convertObject(body, UserRequest.class);
//          userService.processUserRequest(userRequest);
        } catch (Exception e) {
          throw new BadRequestException(ValidationCode.INVALID_REQUEST);
        }
        return ok(Json.toJson(userRequest));
      }
    });
    return response;
  }
}
