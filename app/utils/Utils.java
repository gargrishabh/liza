package utils;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Created by rishabh.garg on 9/10/16.
 */
public class Utils {

  private static final ObjectMapper objMapper = new ObjectMapper();

  public static <T> T convertObject(Object from, Class<T> to) {
    return objMapper.convertValue(from, to);
  }

}
